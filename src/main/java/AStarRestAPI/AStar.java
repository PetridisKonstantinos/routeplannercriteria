package AStarRestAPI;

import java.util.List;

public class AStar
{
    private final long id;
    private final double lat1;
    private final double lon1;
    private final double lat2;
    private final double lon2;
    private final List<List<Double>> path;

    AStar(long id, double lat1, double lon1, double lat2, double lon2)
    {
        this.id = id;
        this.lat1 = lat1;
        this.lon1 = lon1;
        this.lat2 = lat2;
        this.lon2 = lon2;
        Node startNode = new Node("",null, lat1, lon1, AStarRestApiApplication.getNO2ATCoord(lat1, lon1), 0, 0);
        Node endNode = new Node("",null, lat2, lon2, AStarRestApiApplication.getNO2ATCoord(lat2, lon2), 0, 0);
        this.path = AStarRestApiApplication.aStarAssets(startNode, endNode);
    }

    public long getID()
    {
        return id;
    }

    public double getStartX() 
    {
        return lat1;
    }

    public double getStartY() 
    {
        return lon1;
    }

    public double getEndX() 
    {
        return lat2;
    }
    
    public double getEndY() 
    {
        return lon2;
    }
    
    public List<List<Double>> getPath()
    {
        return path;
    }
    
}
package AStarRestAPI;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class AStarController
{
    private final AtomicLong counter = new AtomicLong();
    //User URL includes coordinates for start and end of a route, result is a list of coordinates
    @GetMapping("/aStar")
	public AStar aStar(@RequestParam(value = "lat1") double lat1,
                 @RequestParam(value = "lon1") double lon1,
                 @RequestParam(value = "lat2") double lat2,
                 @RequestParam(value = "lon2") double lon2)
    {
	   return new AStar(counter.incrementAndGet(),lat1,lon1,lat2,lon2);
	}

    //User URL sends asset information as a String message, URL simply returns the object for validation reasons
    //Server parses String message and saves asset information
    @GetMapping("/clientRequest")
    public ClientRequest clientRequest(@RequestParam(value="message")String message)
    {
        return new ClientRequest(counter.incrementAndGet(), message);
    }
}

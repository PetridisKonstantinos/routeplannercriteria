package AStarRestAPI;

import java.util.Comparator;

public class NodeComparator implements Comparator<Node>
{
    /*
    *   Overriding the compare method and adding NO2 concentration data for our criterion.
    *   There should be two weights that change the results of this method.
    *   The NO2 concentration is accumulated to the distance traveled.
    *   General formula: w1*(g + h) + w2*NO2
    *
    *   concentration of nodes * g /2
    */
    @Override
    public int compare(Node a, Node b)
    {
        double aDist = (a.getG() + a.getH());
        double bDist = (b.getG() + b.getH());
        double aConc = a.getnO2Concentration();
        double bConc = b.getnO2Concentration();

        double aHeurNO2 = a.getH() * AStarRestApiApplication.minConc;
        double bHeurNO2 = b.getH() * AStarRestApiApplication.minConc;

        double fScoreA = aDist + aConc;
        double fScoreB = bDist + bConc;

        if (fScoreA>fScoreB)
        {
            return 1;
        }
        else if(fScoreA<fScoreB)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }      
}

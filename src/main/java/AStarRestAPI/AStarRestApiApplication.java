package AStarRestAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://www.youtube.com/watch?v=ySN5Wnu88nE
 */
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.net.URL;
import java.util.*;

import java.lang.Math;

@SpringBootApplication
public class AStarRestApiApplication 
{
	private static PriorityQueue<Node> openList;
    private static List<Node> closedList;
    private static List<Node> assetList;
    private static List<Node> assetListTwo;
    public static Map <String, Node> nodeDictionary;
    public static double[][] sensorData;
    public static int columns,rows;
    // useful variables for normalizing the distance and gas concentration in the comparator
    public static double minConc = 0.0;
    public static double accNo2 = 0.0;

    public double clientConstraint = 0.0;

	public static void main(String[] args) 
	{
		SpringApplication.run(AStarRestApiApplication.class, args);

		// Reading all NO2 concentrations from the RIVM ESRI grid and saving them in an List.
        try 
        {
            URL url = new URL("https://samenmeten.rivm.nl/uurkaart/images/combined_grid_utr_no2.asc");
            Scanner s = new Scanner(url.openStream());
            s.next();
            columns = Integer.parseInt(s.next());
            s.next();
            rows = Integer.parseInt(s.next());
            s.next();
            float xllCRD =  s.nextFloat();
            s.next();
            float yllCRD =  s.nextFloat();
            s.next();
            float cellSize = s.nextFloat();
            s.next();
            s.next();
            sensorData = new double[columns][rows];
            for (int y=0; y<rows; y++)
            {
                for(int x=0; x<columns; x++)
                {
                    if (minConc>sensorData[x][y])
                    {
                        minConc = sensorData[x][y];
                    }
                    sensorData[x][y] = Double.parseDouble(s.next());
                }                
            }       
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        // Reading all Way and Node information from the JSON file and saving them in a list.
        //TODO:Import JSON map files for Darmstadt, Athens, Budapest
        JSONParser jsonParser = new JSONParser();
        nodeDictionary = new HashMap();
        assetList = new ArrayList<>();
        /**
         * The paths should be changed before the route planner starts working.
         * The JSON file in the FileReader holds all the nodes that will be explored in the path planning process.
         * The current one,bikelanesv2.json, is about the road nodes of Utrecht.
         * Use the data mining tool, Overpass Turbo, to extract a similar JSON file with the city of your choice.
         */
        try (FileReader reader = new FileReader("C:\\Users\\kosta\\OneDrive\\Desktop\\AStarRestAPI\\src\\main\\java\\AStarRestAPI\\bikelanesv2.json"))
        {
            Object obj = jsonParser.parse(reader); 
            JSONArray roadNodesList = new JSONArray();
            roadNodesList.add(obj);
            JSONObject tObject = (JSONObject) obj;
            JSONArray elements = (JSONArray) tObject.get("elements");

            elements.forEach( element -> parseRoadNodeObject((JSONObject) element));
            elements.forEach( element -> assignNeighbors((JSONObject) element));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        // Reading all asset node information from the JSON file and saving them in a list.
        JSONParser jsonParserAsset = new JSONParser();
        /**
         * In a similar mindset to the FileReader above, this JSON file holds all node information about a specific
         * type of asset, the benches all around Utrecht. This one can easily be changed to a JSON file of your choice,
         * extracted in a similar manner from Overpass Turbo.
         */

        try (FileReader reader = new FileReader("C:\\Users\\kosta\\OneDrive\\Desktop\\AStarRestAPI\\src\\main\\java\\AStarRestAPI\\benches_utrecht.json"))
        {
            Object obj = jsonParserAsset.parse(reader);
            JSONArray assetNodesList = new JSONArray();
            assetNodesList.add(obj);
            JSONObject tObject = (JSONObject) obj;
            JSONArray elements = (JSONArray) tObject.get("elements");

            elements.forEach( element -> parseAssetNodeObject((JSONObject) element));

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
	}

	private static void parseRoadNodeObject(JSONObject element)
    {
        if (((String)element.get("type")).equals("node"))
        {
            String nodeId = (long) element.get("id") +"";

            double lat = (double) element.get("lat");  
            
            double lon = (double) element.get("lon");

            double nodeConc = getNO2ATCoord(lat, lon);
            Node newRoadNode = new Node(nodeId,null, lat, lon, nodeConc,Double.MAX_VALUE,0.0);
            
            nodeDictionary.put(nodeId,newRoadNode);
        }        
    }


    private static void parseAssetNodeObject(JSONObject element)
    {
        if (((String)element.get("type")).equals("node"))
        {
            String nodeId = (long) element.get("id") +"";

            double lat = (double) element.get("lat");

            double lon = (double) element.get("lon");

            Node newAssetNode = new Node(nodeId, lat, lon, true,"bench",0.0,0.0);
            assetList.add(newAssetNode);
        }
    }


    // Method for checking the neighbors of various nodes and assigning them to each other.
	private static void assignNeighbors(JSONObject element)
    {
        if (((String)element.get("type")).equals("way"))
        {
            JSONArray nodes = (JSONArray) element.get("nodes");

            for(int i=0; i<nodes.size(); i++ )
            {
                long nodeIdLong = (long) nodes.get(i);
                String nodeId = nodeIdLong + "";
                if (i==0)
                {
                    String nodeIdPlus = (long) nodes.get(i+1) + "";
                    nodeDictionary.get(nodeId).addNeighbor(nodeIdPlus);
                }
                else if (i==(nodes.size()-1))
                {
                    String nodeIdMinus = (long) nodes.get(i-1) + "";
                    nodeDictionary.get(nodeId).addNeighbor(nodeIdMinus);
                }
                else
                {
                    String nodeIdPlus = (long) nodes.get(i+1) + "";
                    nodeDictionary.get(nodeId).addNeighbor(nodeIdPlus);
                    String nodeIdMinus = (long) nodes.get(i-1) + "";
                    nodeDictionary.get(nodeId).addNeighbor(nodeIdMinus);
                }                
            } 
        }   
    }

	/// Input a coordinate within Utrecht and get the NO2 from the grid
    public static double getNO2ATCoord(double lati, double longi) 
    {

        /**
         * Reading the Esri grid file, the first lines provide us with two RD coordinates.
         * This website was used for the conversion (https://www.gpscoordinaten.nl/converteer-rd-coordinaten.php).
         * The XLL and YLL centers in GPS coords are: 52.01061, 4.96481
         * The leftmost and the rightmost grid coordinates are selected for these calculations
         * in that way it will resemble the way a list is read or parsed,
         * instead of having the bottom left coordinate or the bottom left center coordinnate as a 0,0 point.
         */

        double LatiBot = 52.01043;        
        double LongiRight = 5.28459;
        
        double LatiTop = 52.17293;
        double LongiLeft = 4.96452;        

        int xCell = (int) Math.round(columns*((longi-LongiLeft)/(LongiRight-LongiLeft)));        
        if (xCell<0)
        {
            xCell = 0;
        }
        else if(xCell>=columns)
        {
            xCell = columns-1;
        }
        int yCell = (int) Math.round(rows-(rows*((lati-LatiBot)/(LatiTop-LatiBot))));
        if (yCell<0)
        {
            yCell = 0;
        }
        else if(yCell>=rows)
        {
            yCell = rows-1;
        }

        return sensorData[xCell][yCell];
    }

	// method for selecting a node with the minimum distance from another
    public static String findClosestPoint(Node a)
    {
        double min = Double.MAX_VALUE;
        String minId = "";
        for (Map.Entry<String, Node> entry : nodeDictionary.entrySet()) 
        {            
            double num = a.euclideanDist(a, entry.getValue());
            if (num<min)
            {
                min = num;
                minId = entry.getKey();
            }
        }
        return minId;
    }
    // Method for cleaning up the open and the closed list after the algorithm is finished.
    public static void cleanLists()
    {
        for(int i=0; i<closedList.size(); i++)
        {
            nodeDictionary.get(closedList.get(i).getId()).setParent(null);
        }

        while(!openList.isEmpty())
        {
            nodeDictionary.get(openList.poll().getId()).setParent(null);
        }
        closedList.clear();
    }

	public static List<List<Double>> aStar(Node start, Node end)
    {
        /**
         * The open list of nodes that are about to be explored should be a Priority queue
         * with a custom comparator that will always keep the node with the lowest f = g+h score.
         * Two of these comparators have been provided, one for NO2 reduction and one for asset optimization,
         * and modes can be switched in the openList initialization.
         *
         * If new criteria are proposed, a custom comparator can easily be created and used here straight away.
         */

        // Initializing priority queue with the custom comparator and the list of the final path
        openList = new PriorityQueue<Node>(11, new NodeComparatorAsset());
        closedList = new ArrayList<>();
        List<List<Double>> pathListCoord = new ArrayList<List<Double>>();
        
        // setting up g and h values for starting and ending node
        start.setG(0.0);
        start.setH(start.euclideanDist(start, end));
        start.setnO2Concentration(getNO2ATCoord(start.getX(),start.getY()));        
        
        end.setG(end.euclideanDist(start, end));
        end.setH(0.0);
        end.setnO2Concentration(getNO2ATCoord(start.getX(),start.getY()));

        // finding actual nodes to represent the start and the end of a route
        // setting up g and h values for both of them
        Node roadNodeStart = nodeDictionary.get(findClosestPoint(start));
        Node roadNodeEnd = nodeDictionary.get(findClosestPoint(end));

        roadNodeStart.setG(0.0);
        roadNodeStart.setH(roadNodeStart.euclideanDist(roadNodeStart, roadNodeEnd));
        roadNodeStart.setF(roadNodeStart.getG()+roadNodeStart.getH());

        roadNodeEnd.setG(Double.MAX_VALUE);
        roadNodeEnd.setH(0.0);
        roadNodeEnd.setF(roadNodeEnd.getG()+roadNodeEnd.getH());
                
        openList.add(roadNodeStart);
        accNo2 = accNo2 + roadNodeStart.getnO2Concentration();

        while(!openList.isEmpty())
        {
            // poll gets and removes the head object of a priority queue
            Node current = openList.poll();
            
            // current node is the ending node, we break the loop and return the path
            if((current.getId().equals(roadNodeEnd.getId())))
            {   
                // return the finished path the algorithm found
                Node n = roadNodeEnd;
                List<String> pathList = new ArrayList<>();
                while(n.getParent() != null)
                {
                    pathList.add(n.getId());
                    n = n.getParent();
                }
                pathList.add(n.getId());
                Collections.reverse(pathList);
                
                for(int i=0; i<pathList.size(); i++)
                {
                    //we should clean up the parents of the nodes, every run should be a brand new one
                    nodeDictionary.get(pathList.get(i)).setParent(null);

                    double nodeX = nodeDictionary.get(pathList.get(i)).getX();
                    double nodeY = nodeDictionary.get(pathList.get(i)).getY();
                    pathListCoord.add(new ArrayList<Double>(Arrays.asList(nodeX,nodeY)));
                }
                cleanLists();
                return pathListCoord;
            }

            closedList.add(current);
            //accNo2 = accNo2 + current.getnO2Concentration();

            // with these calculations here, we check if there's a better path for a node
            for(int i=0; i<current.getListOfNeighbors().size(); i++)
            {
                Node neighbor = nodeDictionary.get(current.getNeighborFromList(i));

                if(closedList.contains(neighbor))
                {
                    continue;
                }

                // tentativeGScore is the distance from start to a neighbor through the current node
                // d is the distance between the current node and a neighbor
                // NO2 calculation (concentration between 2 points/2) * dist
                double d = current.euclideanDist(current, neighbor);
                double tentGScore = current.getG() + d;
                double gScoreNeigh = neighbor.getG();
                if(tentGScore<gScoreNeigh)
                {
                    // we found a better path to a neighbor node through the current, so we save it
                    neighbor.setParent(current);
                    neighbor.setG(tentGScore);
                    if(neighbor.getH()==0)
                    {
                        double hScoreNeigh = neighbor.euclideanDist(neighbor, roadNodeEnd);
                        neighbor.setH(hScoreNeigh);
                    }
                    neighbor.setF(neighbor.getG()+neighbor.getH());
                    if(!openList.contains(neighbor))
                    {
                        openList.add(neighbor);
                    }
                }
            }           
        }
        cleanLists();
        return pathListCoord;
    }

    /**
     *  This method for asset optimization is a recursive A* algorithm, which changes the starting and the ending
     *  point of a route, in order to create a route between various types of assets.
     */
    public static List<List<Double>> aStarAssets(Node start, Node end)
    {
        assetListTwo = new ArrayList<>();
        assetListTwo = initializeAssetList(start, end, start.euclideanDist(start,end));
        List<List<Double>> current = new ArrayList<List<Double>>();
        List<List<Double>> path = new ArrayList<List<Double>>();

         for(int i = 0; i<assetListTwo.size()-1; i++)
         {
              current = aStar(assetListTwo.get(i),assetListTwo.get(i+1));
              if (current.size()==2)
              {
                  System.out.println("Curent variable is size of 2 with i=" +i+
                          " Start: "+assetListTwo.get(i).getX()+","+assetListTwo.get(i).getY()+
                          " End: "+assetListTwo.get(i+1).getX()+","+assetListTwo.get(i+1).getY());
              }
              else if (current.size()==1)
              {
                  System.out.println("Curent variable is size of 1 with i=" +i+
                          " Start: "+assetListTwo.get(i).getX()+","+assetListTwo.get(i).getY()+
                          " End: "+assetListTwo.get(i+1).getX()+","+assetListTwo.get(i+1).getY());
              }
              else if (current.size()==0)
              {
                  System.out.println("Curent variable is size of 0 with i=" +i+
                          " Start: "+assetListTwo.get(i).getX()+","+assetListTwo.get(i).getY()+
                          " End: "+assetListTwo.get(i+1).getX()+","+assetListTwo.get(i+1).getY());
              }
              path.addAll(current);
         }

         for(int j=0; j<path.size()-1; j++)
         {
              if(path.get(j).equals(path.get(j+1)))
              {
                  //System.out.println("Duplicates found in position "+j+ " and "+ (j+1));
                  path.remove(j+1);
              }
         }
        return path;
    }

    // Parsing the String message received by the client's request and saving asset nodes in a list
    public static List parseClientRequest(String clientRequest)
    {
        List list = new ArrayList<>();
        // Removing all unecessary substrings, so as to get a String format easily separated with the comma delimiter
        clientRequest = clientRequest.replace("type:","");
        clientRequest = clientRequest.replace("lat:","");
        clientRequest = clientRequest.replace("lon:","");
        clientRequest = clientRequest.replace("constraint:","");

        Scanner scanner = new Scanner(clientRequest);
        scanner.useDelimiter(",");
        double helpDouble = 0.0;
        String helpString = "";
        while (scanner.hasNext())
        {
            if(scanner.hasNextDouble())
            {
                helpDouble = scanner.nextDouble();
                list.add(helpDouble);
            }
            else
            {
                helpString = scanner.next();
                list.add(helpString);
            }
        }
        return list;
    }
    // Getting the last entry of the previous list which is the constraint posed by the client.
    public static double getClientConstraint(List list)
    {
        return (double) list.get(list.size()-1);
    }
    //Setting up the previous client request list as a list of nodes, ready to be preprocessed.
    //TODO:Test this function
    public static List<Node> setupNodeList(List list)
    {
        List<Node> nodeList = new ArrayList();
        list.remove(list.size()-1);
        for(int i=0;i<list.size();i+=3)
        {
            Node node = new Node("",(double)list.get(i+1),(double)list.get(i+2),true,(String)list.get(i),0.0,0.0);
            nodeList.add(node);
        }
        return nodeList;
    }
    // Preprocessing of assets based on the constraint requested from the client
    public static List<Node> initializeAssetList(Node startNode, Node endNode, double maxDist)
    {
        List<Node> list = new ArrayList<>();
        list.add(startNode);
        double midPointX = (startNode.getX() + endNode.getX())/2;
        double midPointY = (startNode.getY() + endNode.getY())/2;
        double midPointRadius = maxDist * 0.5;
        for (Node node:assetList)
        {
            double xDist = node.getX() - midPointX;
            double yDist = node.getY() - midPointY;
            double nodeDist = Math.sqrt((Math.pow(xDist, 2))+(Math.pow(yDist,2)));
            double distFromEnd = node.euclideanDist(node,endNode);
            if(nodeDist<midPointRadius && distFromEnd<maxDist)
            {
                list.add(node);
            }
            maxDist = distFromEnd;
        }
        list.add(endNode);
        return list;
    }
}

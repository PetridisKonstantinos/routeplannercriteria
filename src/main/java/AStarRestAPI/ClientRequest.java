package AStarRestAPI;

import java.util.List;

public class ClientRequest
{
    private long id;
    private String message;
    private List assetList;
    private double constraint;

    ClientRequest(long id, String message)
    {
        this.id=id;
        this.message=message;
        this.assetList = AStarRestApiApplication.parseClientRequest(message);
        this.constraint = AStarRestApiApplication.getClientConstraint(assetList);
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String newMessage)
    {
        message=newMessage;
    }

    public long getId()
    {
        return id;
    }

    public double getConstraint() {return constraint;}

    public List getAssetList() {
        return assetList;
    }

}

package AStarRestAPI;

import java.util.Comparator;

public class NodeComparatorAsset implements Comparator<Node>
{
    @Override
    public int compare(Node a, Node b)
    {
        if (a==null)
        {
            System.out.println("A is Null");
        }
        double fScoreA = (a.getG() + a.getH());
        double fScoreB = (b.getG() + b.getH());

        if (fScoreA>fScoreB)
        {
            return 1;
        }
        else if(fScoreA<fScoreB)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
}

package AStarRestAPI;

import java.util.ArrayList;
import java.util.List;

public class Node implements Comparable
{
    private Node parent;
    private double x, y;
    private double g;
    private double h;
    private double f;
    private String id;
    private List<String> neighborList;
    private double nO2Concentration;
    private boolean isAsset;
    private String assetType;

    Node(){}

    Node(String id,double xpos, double ypos)
    {
        this.id = id;
        this.x = xpos;
        this.y = ypos;
    }
    
    Node(String id,Node parent, double xpos, double ypos, double nO2Concentration,double g, double h)
    {
        this.id = id;
        this.x = xpos;
        this.y = ypos;
        this.nO2Concentration = nO2Concentration;
        this.g = g;
        this.h = h;
        this.f = g + h;
        this.neighborList = new ArrayList<String>();
    }

    Node(String id, double xpos, double ypos, boolean isAsset, String assetType,double g, double h)
    {
        this.id = id;
        this.x = xpos;
        this.y = ypos;
        this.isAsset = isAsset;
        this.g = g;
        this.h = h;
        this.f = g + h;
        this.assetType = assetType;
    }

    public String getId() {
        return id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public double getG() {
        return g;
    }
    public void setG(double g) {
        this.g = g;
    }
    public double getH() {
        return h;
    }
    public void setH(double h) {
        this.h = h;
    }

    public void setF(double f) {
        this.f = f;
    }

    public double getF() {
        return f;
    }

    public boolean isAsset() {
        return isAsset;
    }

    public void setAsset(boolean asset) {
        isAsset = asset;
    }

    public double getnO2Concentration() {
        return nO2Concentration;
    }

    public void setnO2Concentration(double nO2Concentration) {
        this.nO2Concentration = nO2Concentration;
    }

    public void addNeighbor(String neighborId)
    {
        neighborList.add(neighborId);
    }

    public List<String> getListOfNeighbors()
    {
        return neighborList;
    }

    public String getNeighborFromList(int index)
    {
        return neighborList.get(index);
    }

    public String getAssetType()
    {
        return assetType;
    }

    public void setAssetType(String assetType)
    {
        this.assetType = assetType;
    }
    // Compare by f value (g + h)
    @Override
    public int compareTo(Object o) 
    {
       Node that = (Node) o;
       return (int)((this.g + this.h) - (that.g + that.h));
    }
    
    // Calculation of euclidean distance between two nodes
    public double euclideanDist(Node a, Node b)
    {
       double xRes = b.getX() - a.getX();
       double yRes = b.getY() - a.getY();
       double xPY = (Math.pow(xRes, 2))+(Math.pow(yRes,2));

       return Math.sqrt(xPY);
    }
}
